# Pythono3 code to rename multiple
import os
import glob


def main():
    i = 0
    caminho = "/home/alvaro/Desktop/object_detection_demo/data/images_mineiro/test/"

    for arquivo in glob.glob(os.path.join(caminho, "*.xml")):
        new_file = caminho + "bixo" + str(i) + ".xml"
        os.rename(arquivo, new_file)
        i += 1


if __name__ == '__main__':
    main()
